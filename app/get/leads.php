<?php
	ini_set('display_errors', 1);
	ini_set('display_startup_errors', 1);
	ini_set('error_reporting', -1);

	include $_SERVER['DOCUMENT_ROOT']. '/includes/header.php';
?>

<form action="" method="post" class="getLeadsForm">
	<label for="value">Enter Value to Test:</label>
	<input type="text" name="value" id="value" />
</form>

<button type="button" class="btn btn-submit valueSubmitBtn" id="leadsBtn">
	Submit
	<i class="fa fa-fw fa-send"></i>
</button>

<div id="loadResults" class="hidden"><pre class="prettyprint" id="load"></pre></div>

<?php include $_SERVER['DOCUMENT_ROOT']. '/includes/footer.php'; ?>
