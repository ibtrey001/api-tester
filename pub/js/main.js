$(document).ready(function()
{
	$('#leadsBtn').on('click', function()
	{
		var val = $('#value').val();

		$.ajax({
			data:     {value: val},
			dataType: 'json',
			type:     'post',
			url:      '/app/ajax/get/leads.php',
			success:  function(res) {$('#load').html(JSON.stringify(res), null, 4); $('#loadResults').removeClass('hidden')},
			error:    function(res) {console.log(res)}
		})
	});
});
