<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8" />
	<title>API Tester</title>

	<link rel="stylesheet" href="/pub/css/plugins/bootstrap.min.css" />
	<link rel="stylesheet" href="/pub/css/plugins/font-awesome.min.css" />
	<link rel="stylesheet" href="/pub/css/style.css" />
</head>
<body>
	<header>
		<h1>API Tester</h1>
		<p>Version: 2017-07.1
	</header>

	<div class="container">
		<div class="menu">
			<nav>
				<ul>
					<li class="menuLink">
						get

						<ul><li class="activeLink"><a href="<?php echo '/app/get/leads.php'; ?>"/>leads</a></li></ul>
					</li>
					<li class="menuLink">post</li>
				</ul>
			</nav>
		</div>

		<div class="content">
